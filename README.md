<h1>UoE Survey Scripts</h1>

To insert the Uoe Survey into your website, please do the following:

<h2>Javascript</h2>

<h3>jQuery</h3>
jquery 1.12 has been included in the zip file. This is required to make the pop up function properly. If you already have bootstrap in your website, this file wont be needed

<h3>uoeSurveyScript_web.js</h3>

A link to this file should be placed in the header section of your website after any jQuery libraries or bootstrap is called. This file shouldn't’t be modified.

Please note it will set a cookie tied to the .ed.ac.uk domain name to allow users to move between .ed.ac.uk websites without seeing the survey again. The cookie has been set to expire after 3 months.

<h3>uoeSurveyScriptFooterXXXX.js</h3>
There are several copies of this file. Each has been modified to link to that specific survey ie:

uoeSurveyScriptFooterSchools.js will link to the schools survey (https://www.research.net/r/uoe_schools)

If you need to modify this for a new survey, please edit the following variable to point to the url of your survey:

<code>var uoeSurveyURL = 'https://www.research.net/r/uoe_global';</code>

Once your script is ready, link to it in your page footer just above the closing <strong>body</strong> tag. Footer.

<h2>Styling (CSS)</h2>

<h3>uoeSurveyScript.css</h3>
A link to this file should be placed in the header section of your website after all other CSS has been called. This script is independent of any libraries such as EdGel and will use the font on your website.

Your survey should now appear in the centre of the screen with a faded background.

<h2>Support</h2>

If you have any issues installing the script, please contact <a href="mailto:website.support@ed.ac.uk">website.support@ed.ac.uk</a>.

