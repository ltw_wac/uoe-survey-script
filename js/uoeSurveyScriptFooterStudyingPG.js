/*
 *   Survey Trigger JS Script
 */

(function ($) {

  var uoeSurveyURL = 'https://www.research.net/r/uoe_studying-pg';

  uoeSurveyScript.cookieName = "_uoeSurveyStatus";
  uoeSurveyScript.cookieValue = getUoECookieSettings(uoeSurveyScript.cookieName);

  if (!uoeSurveyScript.cookieValue) {
    uoeSurveyScript.insertPanel(uoeSurveyURL);
    $('body').prepend('<span class="uoe-website-mask"></span>');
  }

})(jQuery);