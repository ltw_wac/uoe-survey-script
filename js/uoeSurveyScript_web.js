/*
 *   Survey Creation JS Script
 */
var uoeSurveyScript = {};

(function ($) {

  "use strict";

  $(document).ready(function () {

    // on No button click or clicking the cross, hide the panel
    $('#uoe-survey-panel-close-btn, #uoe-survey-no').click(function() {
      $('#uoe-survey-panel-notice').removeClass('uoe-survey-panel-show').addClass('uoe-survey-panel-hide');
      $(".uoe-website-mask").remove();
    });
    // if the user clicks yes or no set the cookie so that the banner doesn't load again
    $('#uoe-survey-yes, #uoe-survey-no').click(function() {
      uoeSurveyScript.createCookie('_uoeSurveyStatus','1','90');
        $(".uoe-website-mask").remove();
    });
  });
})(jQuery);

// create the survey panel
uoeSurveyScript.insertPanel = function (surveyURL) {

  "use strict";

  uoeSurveyScript.surveyURL = surveyURL;

  // check if the cookie is set, if not display the noticePanel
  if (document.cookie.indexOf('uoeSurveyScript') < 0) {
    // add the structured panel here

    document.writeln('<div id="uoe-survey-panel-notice" class="uoe-survey-panel-show">');
    document.writeln('<div class="uoe-survey-panel">');
    document.writeln('<span id="uoe-survey-panel-close-btn" role="button" class="uoe-survey-panel-close"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 512 512">\n' +
        '<g>\n' +
        '</g>\n' +
        '\t<path d="M256.010 204.645l100.118-100.146 51.344 51.33-100.118 100.146-51.344-51.329z" fill="#000000" />\n' +
        '\t<path d="M155.827 407.483l-51.344-51.358 100.161-100.132 51.344 51.358-100.161 100.132z" fill="#000000" />\n' +
        '\t<path d="M407.498 356.112l-51.373 51.358-100.118-100.146 51.373-51.358 100.118 100.146z" fill="#000000" />\n' +
        '\t<path d="M104.502 155.857l51.337-51.351 100.153 100.125-51.337 51.351-100.153-100.125z" fill="#000000" />\n' +
        '\t<path d="M255.983 307.36l-51.351-51.365 51.365-51.351 51.351 51.365-51.365 51.351z" fill="#000000" />\n' +
        '</svg></span>');
    document.writeln('<span id="uoe-survey-panel-text-header">Interested in studying with us? Take our survey to help us improve our website!</span>');
    document.writeln('<span id="uoe-survey-panel-text">Take our short survey and you could be in with a chance of winning one of four £50 Amazon vouchers. Your answers will help us improve our website for prospective students.</span>');
    document.writeln('<ul class="list-inline">\n' +
        '  <li><a id="uoe-survey-yes" href="'+ surveyURL + '" class="uoe-survey-btn uoe-survey-btn-primary" role="button">Yes, take me to the survey</a>' +
        '  <li><button id="uoe-survey-no" type="button" class="uoe-survey-btn uoe-survey-btn-secondary"> No, take me back to the website</button></li>\n' +
        '</ul>');
    document.writeln('</div>');
    document.writeln('</div>');
  }
};

// create the survey cookie
uoeSurveyScript.createCookie = function (uoeCookieName,uoeCookieValue,cookieExpiryDays) {

  "use strict";

  uoeSurveyScript.expires = '';
  uoeSurveyScript.name = uoeCookieName;
  uoeSurveyScript.value = uoeCookieValue;
  uoeSurveyScript.domain = '.ed.ac.uk';
  uoeSurveyScript.path = '/';
  uoeSurveyScript.expiryDays = cookieExpiryDays;
  uoeSurveyScript.date = new Date();

  // set the current path
  uoeSurveyScript.currentLocation = window.location.pathname;
  uoeSurveyScript.currentFolder = uoeSurveyScript.currentLocation.substring(0, uoeSurveyScript.currentLocation.lastIndexOf('/'));

  // generate the expiry date
  if (uoeSurveyScript.expiryDays) {
    uoeSurveyScript.date.setTime(uoeSurveyScript.date.getTime() + (uoeSurveyScript.expiryDays * 24 * 60 * 60 * 1000));
    console.log(uoeSurveyScript.date);
    uoeSurveyScript.expires = ";  expires=" + uoeSurveyScript.date.toUTCString();
  }
  document.cookie = uoeSurveyScript.name + "=" + uoeSurveyScript.value + uoeSurveyScript.expires + "; domain=" + uoeSurveyScript.domain + "; path=" + uoeSurveyScript.path;

};

// check the survey cookie exists
function getUoECookieSettings(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

